const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

/*
txtLastName.addEventListener("keyup", (event) => {

	let fullNameResult = txtFirstName.value.concat("   ", txtLastName.value)
	spanFullName.innerHTML = fullNameResult;
})
*/
function updateFullname(){
	spanFullName.innerHTML =`${txtFirstName.value} ${txtLastName.value}`;
}

txtFirstName.addEventListener('keyup', updateFullname);
txtLastName.addEventListener('keyup', updateFullname);